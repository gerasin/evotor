;(function($) { 
	/*fancybox*/
	/*if($('.js-popup-link').length) {
		$('.js-popup-link').fancybox({
			autoFocus : false,
			touch: false
		});
	}*/
    
    /*spinner*/
	if($('.js-spinner').length) {
		$('.js-spinner').spinner();
	}
	
	if ($('[data-fancybox]').length) {
		$('[data-fancybox]').fancybox({
			autoFocus : false,
			touch: false
		});
	}
	
	if ($('.js-popup-close').length) {
		$('.js-popup-close').on('click', function(e) {
			$.fancybox.close(true);
		})
	}
    
    function tabs() {
		var $tabContainer = $('.js-tabs-container');
		
		if ($tabContainer.length) {
			$tabContainer.each(function() {
				var self = $(this),
					$tab = self.find('.js-tab'),
					$content = self.find('.js-tab-content');
				
				$tab.on('click', function(e) {
					var index = $(this).index();
					
					$tab.removeClass('active');
					$(this).addClass('active');
					$content.removeClass('active').eq(index).addClass('active');
				});
			})
		}
 	}
	tabs();
	
}($));
	
