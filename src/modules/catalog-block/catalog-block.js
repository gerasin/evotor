;(function() {
	
	function declOfNum(titles){
		var number = Math.abs(number);
		var cases = [2, 0, 1, 1, 1, 2];
		return function(number){
			return  titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
		}
	}
	
	
	function addToCart() {
		var $block = $('.js-prod-card'),
			$panel = $('.js-cart-panel'),
			list = $('.js-cart-panel-list'),
			sumElem = $('.js-cart-panel-sum'),
			sum = +sumElem.text();
			
		
		if ($block.length) {
			$block.each(function() {
				var self = $(this),
					addCart = self.find('.js-add-cart'),
					price = +self.find('.js-prod-price').text().replace(/\s*/g,''),
					title = self.find('.js-prod-title');
					

				function addToList() {
					
					if (list.find('li').length < 3) {
						list.append('<li class="cart-panel__list-item">' + title.text() + '</li>');
					} else {
						var count = +$('.js-cart-panel-count').text();
						
						$('.js-cart-panel-more')
							.addClass('visible')
							.find('.js-cart-panel-count')
							.text(count + 1)
							.siblings('.js-cart-panel-count-text')
							.text(declOfNum(['товар','товара','товаров'])(count + 1));
					}
				}
				
				function addSum() {
					var sum = +sumElem.text().replace(/\s*/g,'') + price;

					sumElem.text(sum.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
				}
				
				addCart.on('click', function(e) {
					e.preventDefault();

					if (!$panel.is(':visible')) {
						$panel.fadeIn(200, function() {
							self.addClass('in-basket');
							$panel.addClass('visible');
							addToList();
							addSum();
						});
					} else {
						self.addClass('in-basket');
						addToList();
						addSum();
					}
						
				})

			})
		}
	}
	
	addToCart();
	
	function stickyPanel() {
		var $footer = $('.footer'),
			$panel = $('.js-cart-panel');
		if ($panel.length) {
			$(window).on('scroll', function() {
				var wHeight = $(window).outerHeight(),
					wTop = $(window).scrollTop(),
					footerTop = $footer.offset().top;
				
				if (!$panel.hasClass('relative')) {
					if (wHeight + wTop >= footerTop) {
						$panel.addClass('relative');
					}
				} else {
					if (wHeight + wTop < footerTop) {
						$panel.removeClass('relative');
					}
				}
			})
		}
	}
	
	stickyPanel();
	
}());