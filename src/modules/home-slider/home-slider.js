;(function() {

	function home_slider() {
        if($('.js-home-slider').length) {
            $('.js-home-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: true,
                fade: true,
                infinite: true,
                //adaptiveHeight: true
                //autoplay: true
            });
        }
    }
    home_slider();
}())